# Debian developer tools installer.

It is a simple shell script to install some developer tools in a new Debian 11 local machine.

## Warnings:

* The script is designed for machines with freshly installed Debian 11.
* The script is designed to run linearly.
* My objective was : ALL Tools I could need in a new Debian installation after execute a sh.
* In this folder you can find a example of /etc/apt/sources.list to execute the script. BUT (The script can run with the original freshly sources.list without the non free indicatives.)
* YOU HAVE TO BE CAREFULL, PLEASE READ THE SCRIPT in debiantoolsdevinstaller.sh !!!
* This needs docker to do easy things: some of the first tools installed by the script is docker.
* The script need you give some inputs like passwords to create postgres docker container; then: you have to monitor the script execution.

## Instaled tools:

The instaled tools are:

- ✨Git and git-cola: Installation on DEBIAN OS✨

- ✨Docker: Installation on DEBIAN OS✨

- ✨Kernel-based Virtual Machine. > To Play with Virtual Machines: Installation on DEBIAN OS✨

- ✨Databases.✨
  * DBeaver    > Free multi-platform database tool for developers: Installation on DEBIAN OS.
  * Postres    > Using Docker Container not direct installation on DEBIAN OS.
  * Marìa DB   > Using Docker Container not direct installation on DEBIAN OS.
  * Redis      > Using Docker Container not direct installation on DEBIAN OS.
  * Neo4j      > Using Docker Container not direct installation on DEBIAN OS.

- ✨Programming languages:✨
  * Python > Installation on DEBIAN OS : Using pyenv to manage multiple Python Versions.
  * Golang > Installation on DEBIAN OS.
  * Rust   > Installation on DEBIAN OS.

- ✨Developer Complements:✨
  * Npm > Installation on DEBIAN OS.
  * Node > Installation on DEBIAN OS.
  * Byobu > Installation on DEBIAN OS.
  * Atom > Installation on DEBIAN OS.

## How to use ?:
FIRST: You have to READ THE ALL THE SCRIPT, in:
- https://gitlab.com/deBmichel/osdtls/-/raw/main/devenv/debiantoolsdevinstaller.sh

then:
- wget https://gitlab.com/deBmichel/osdtls/-/raw/main/devenv/debiantoolsdevinstaller.sh

now:

1. You have to give execution permissions, the next command should function:
- chmod +x debiantoolsdevinstaller.sh

2. You run the sh script in the NEW and FRESHLY Debian 11 installation using the command:
- ./debiantoolsdevinstaller.sh

3. The script needs you give some inputs like your selected password to create postgrescontainer.

4. MONITOR the script execution.
