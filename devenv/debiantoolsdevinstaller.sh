#! /bin/sh

# 1 Some message about the script:
printf "%s\n" "                                                  ________              "
printf "%s\n" "  debiantoolsdevinstaller:                    _ggDBDBDBDDjgg_           "
printf "%s\n" "                                           .gBDBBDDBDBDBDBDBDg.         "
printf "%s\n" "     micro installer for                 ;gSSSSSSSSSSSSSSSSDDDBB.       "
printf "%s\n" "     developers tools on                gBSSP\"\"\"\"       \`s.\"SSDDD.      "
printf "%s\n" "     debian.                           'SSP'\"'             \`AAA.\"B      "
printf "%s\n" "                                      *LSSP\"      ,ggs.     \`SSb:\"      "
printf "%s\n" "                                      BdSS'     ,SD\"'   .    SSS\"!      "
printf "%s\n" "                                      BSSB      dS'     ,    SSD.       "
printf "%s\n" "         It is an easy tool, you      BSS:      SS.   -    ,dSS'        "
printf "%s\n" "         you should read the          BSS;      YSb._   _,dSD'          "
printf "%s\n" "         script.                       BSS.      \`\"YSSSSD\"'             "
printf "%s\n" "                                       \`SSb                             "
printf "%s\n" "                                        \`YSS                            "
printf "%s\n" "                                         \`YSS.                          "
printf "%s\n" "               Designed for fresh          \`SSb.                        "
printf "%s\n" "               debian installations.          \`YSSb.                     "
printf "%s\n" "                                                \`\"YSb._                 "
printf "%s\n" "                                                     \`\"Yb._              "
echo "                                                         \"\"\"-              Good luck $(whoami) !     "
printf "%s\n" "                                                              \"\"-- .  "

# Taking the input to decide execution.
echo "\n\n                I have read the script, the README.md and I want to proceed."
echo "                !!!The script will try to install the ALL tools listed in the README.md!!!"
echo "                are you sure to proceed ?"
printf "                (yes , y) or (not , n): "
read RUN
echo "\n\n"

# Validating input out of valid options
if
  [ $RUN != "n" ] && [ $RUN != "not" ] && [ $RUN != "y" ] && [ $RUN != "yes" ]
then
  echo 'Invalid option !!: valid options are : yes , y , not , n'
  echo 'aborting execution, nothing will be installed.'
  exit 0
fi

# Validating NOT execution.
if
  [ $RUN = "n" ] || [ $RUN = "not" ]
then
    echo 'You have stopped the execution:'
    echo 'aborting execution, nothing will be installed.'
    echo $(whoami)
    exit 0
fi

# Validating YES execution.
if
  [ $RUN = "y" ] || [ $RUN = "yes" ]
then
    echo 'You have decided start the execution:'
    echo 'The script will try to install the ALL tools listed in the README.md'
fi

# showNewInstall: a minor function helper to display new tool installation.
# and the source from which the steps to install a tool were taken.
showNewInstallTry(){
  echo "======================================================================================="
  echo "||****** Tools Installer will try install: $1"
  echo "======================================================================================="
  echo "|| steps taken from :"
  echo "|| $2"
  echo "======================================================================================="
}

# A function to install python version using pyenv
installpythonversionwithpyenv(){
  showNewInstallTry "python $1 using pyenv" "https://github.com/pyenv/pyenv#install-additional-python-versions"
  echo "the install could take 7 minutes; started At: $(date)"
  pyenv install $1
}

echo "starting...."
startdate="Started AT : $(date)"
echo $startdate

# Installing some needed packages:
showNewInstallTry "apt-transport-https ca-certificates curl gnupg lsb-release" "sh creator"
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y

# Installing needed sources and repositories:
showNewInstallTry "needed repositories and external sources to execute os update and upgrade:" "sh creator"

#Docker sources:
echo ">>>> >>> >> > preparing docker sources and repositories"
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

#DBEAVER sources:
echo ">>>> >>> >> >Preparing DBEAVER sources and repositories"
sudo  wget -O /usr/share/keyrings/dbeaver.gpg.key https://dbeaver.io/debs/dbeaver.gpg.key
echo "deb [signed-by=/usr/share/keyrings/dbeaver.gpg.key] https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list

#Atom Sources:
echo ">>>> >>> >> >Preparing Atom sources and repositories"
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'

echo ">>>> >>> >> >Running update and upgrade with all new sources..."
sudo apt-get update && sudo apt-get dist-upgrade -y

# Installing tools:
showNewInstallTry "All tools:" "sh creator"

# Installing git and Byobu
showNewInstallTry "git-all git-core git-cola byobu" "https://github.com/git-guides/install-git  https://git-cola.github.io/downloads.html  https://www.byobu.org/downloads"
sudo apt-get install git-all git-core git-cola byobu -y

# Pyenv for Python Programming Language::
showNewInstallTry "Pyenv to manage multiple python versions" "https://github.com/pyenv/pyenv#basic-github-checkout https://gist.github.com/trongnghia203/9cc8157acb1a9faad2de95c3175aa875"
echo 'Lzma and other packages are needed to install python versions like 3.8.0'
sudo apt install gcc g++ make zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev libssl-dev lzma liblzma-dev tk-dev -y
echo 'Adding libffi6 to solve broken python 3.8.0 installation'
sudo apt-get install libffi-dev
echo "Installing pyenv"
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
printf '\n' >> ~/.bashrc
echo 'echo "Adding pyenv to the terminal"'
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
echo 'echo "Adding pyenv to the terminal"' >> ~/.bashrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

# Installing Atom:
showNewInstallTry "Atom" "https://flight-manual.atom.io/getting-started/sections/installing-atom/"
echo "You can review the #Atom Sources: section in the script "
sudo apt-get install atom

# Installing Dbeaver:
showNewInstallTry "Dbeaver" "https://dbeaver.io/download/"
echo "You can review the #DBEAVER sources: section in the script "
sudo apt-get install dbeaver-ce

# Installing Docker:
showNewInstallTry "Docker" "https://docs.docker.com/engine/install/debian/"
echo "You can review the #Docker sources: section in the script "
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

# Installing Python 3.8.0
installpythonversionwithpyenv "3.8.0"

# Installing postgres using docker:
showNewInstallTry "postgres : (uSING dOCKER)" "https://hub.docker.com/_/postgres  https://www.postgresql.org/docs/current/runtime-config-connection.html"
echo "Please enter a password for user postgres"
read POSTGRESUSER
sudo docker run --name postgrescontainer -p 5432:5432 -e POSTGRES_PASSWORD=$POSTGRESUSER -d postgres
echo "postgres running on 0.0.0.0 PORT 5432 with user: postgres  password: $POSTGRESUSER"

# Installing Python 3.10.6
installpythonversionwithpyenv "3.10.6"

# Installing Marìa DB using docker:
showNewInstallTry "Marìa DB : (uSING dOCKER)" "https://hub.docker.com/_/mariadb   https://mariadb.com/kb/en/configuring-mariadb-for-remote-client-access/#port-3306-is-configured-in-firewall"
echo "Please enter a password for marìa db root user:"
read ROOTUSER
echo "Please enter a new user to marìa db : example : example-user"
read NEWMDBUSER
echo "Please enter a new user PASSWROD to marìa db : example : example-user-password"
read NEWMDBUSERPASSWD
sudo docker run --detach --name mariadbcontainer -p 3306:3306 --env MARIADB_USER=$NEWMDBUSER --env MARIADB_PASSWORD=$NEWMDBUSERPASSWD --env MARIADB_ROOT_PASSWORD=$ROOTUSER  mariadb:latest
echo "María DB running on 0.0.0.0 PORT 3306 with user: root  password: $ROOTUSER"

# Rust Programming Language:
showNewInstallTry "Rust Programming Language" "https://www.rust-lang.org/tools/install"
printf '\n' >> ~/.bashrc
echo 'echo "Adding rust to the terminal"' >> ~/.bashrc
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
echo "setting rust and cargo in the actual terminal"
. "$HOME/.cargo/env"
rustc --version
cargo --version

# Installing Redis using docker:
showNewInstallTry "Redis : (uSING dOCKER)" "https://hub.docker.com/_/redis  https://redis.io/docs/getting-started/"
sudo docker run --name rediscontainer -p 6379:6379 -d redis redis-server --save 60 1 --loglevel warning
echo "redis running on 0.0.0.0 PORT 6379"

# Golang Programming Language:
showNewInstallTry "Golang Programming Language" "https://go.dev/doc/install"
wget https://go.dev/dl/go1.18.4.linux-amd64.tar.gz
echo "Installing golang programming language after download:"
sudo tar -C /usr/local -xzf go1.18.4.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
printf '\n' >> ~/.bashrc
echo 'echo "Adding golang to the path:"' >> $HOME/.bashrc
echo 'export PATH=$PATH:/usr/local/go/bin' >> $HOME/.bashrc
go version
rm go1.18.4.linux-amd64.tar.gz

# Installing neo4j using docker:
showNewInstallTry "neo4j : (uSING dOCKER)" "https://hub.docker.com/_/neo4j"
sudo docker run \
    --detach --name neo4jcontainer \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    neo4j
echo "neo4j runing on 0.0.0.0 PORT 7474"

# Installing KVM:
showNewInstallTry "KVM" "https://www.linuxtechi.com/install-configure-kvm-debian-10-buster/"
printf 'The installation is really functional on Debian 11, but, you can go and review the source :\n'
printf 'https://wiki.debian.org/KVM\nyou can test the kvm installation using the command : virt-manager'

support="$(egrep -c '(vmx|svm)' /proc/cpuinfo)"
echo "KVM SUPPORT = $support, KVM SUPPORT SHOULD BE EQUAL OR GREATER THAN : 4"
if [ $support -ge 4 ]
then
  echo 'Your machine really supports : trying install KVM'
  sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virtinst libvirt-daemon virt-manager -y
  sudo systemctl status libvirtd.service
  sudo virsh net-list --all
  sudo virsh net-start default
  sudo virsh net-autostart default
  sudo modprobe vhost_net
  echo "vhost_net" | sudo  tee -a /etc/modules
  lsmod | grep vhost
  me="$(whoami)"
  sudo adduser $me libvirt
  sudo adduser $me libvirt-qemu
  newgrp libvirt
  newgrp libvirt-qemu
else
  echo '!!!!!! Your machine does not support virtualization or you have to enable virtualization using BIOS'
  echo "!!!!!! KVM won't be installed using this script"
fi

echo "finished...."
finishdate="Finished AT : $(date)"

echo "times:"
echo $startdate
echo $finishdate

exec bash
